<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="style.css" rel="stylesheet" type="text/css">
        <title>ユーザ管理</title>
        <script type="text/javascript">
	    <!--

	    function userStop(){
        	if(window.confirm("停止しますか？")){
        		return true;
        	}else{
        		return false;
        	}
        }


        function revival(){
        	if(window.confirm("復活しますか？")){
        		return true;
        	}else{
        		return false;
        	}
        }
	    //-->
	    </script>


	    <style type="text/css">
	   </style>

	</head>
	<body>
		<div class="header">
			<div class="item">
				<div class="item-name">

					<a href="./" class="btn-border"><span>ホーム</span></a>
				</div>
			</div>

			<div class="item">
				<div class="item-name">
					<a href="signup" class="btn-border"><span>新規登録</span></a>
				</div>
			</div>
		</div>
<br>




		<div class="main-contents">
		<div class="um-main">

			<c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="message">
			                <li><c:out value="${message}" />
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session"/>
			</c:if>
<br>
<br>
<br>
<br>
<br>
		<div class="form-area">
			<div class="posts">
				<c:forEach items="${users}" var="user" >
					<div class="user">
						<div class="profile">

	<!--						<label for="id">ID:
							<div class="id"><c:out value="${user.id}" /></div>
	-->
							<label for="name">名前
							<div class="name"><h3><c:out value="${user.name}" /></h3></div>
							<label for="account">ログインID
							<div class="account"><h3><c:out value="${user.account}" /></h3></div>
	<!--						<label for="password">パスワード <br />
							<div class="password"><c:out value="${user.password}" /></div>
	-->

							<c:forEach items="${branches}" var ="branch">
								<div class = "branch">

									<c:if test = "${branch.id == user.branch_id}" >
	<!--									<label for="branch_id">支店コード <br />
										<div class="branch_id"><c:out value="${branch.id}" /></div>
	-->
										<label for="branch_id">支店名
										<div class="branchName"><h4><c:out value="${branch.name}" /></h4></div>
									</c:if>
								</div>
							</c:forEach>


							<c:forEach items="${positions}" var ="position">
								<div class = "position">

									<c:if test = "${position.id == user.position_id}" >
	<!--									<label for="position_id">部署・役職コード <br />
										<div class="position_id"><c:out value="${position.id}" /></div>
	-->
										<label for="position_id">部署・役職<br />
										<div class="positionName"><h4><c:out value="${position.position}" /></h4></div>
									</c:if>
								</div>
							</c:forEach>


<!--  							<label for="creaated_date">登録日時 <br />
							<div class="date"><fmt:formatDate value="${user.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br>
-->
							<label for="updated_date">更新日時<br />
							<div class="dat"><fmt:formatDate value="${user.updatedDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>



						<div class="botton">
							<c:if test = "${user.id != user_id}" >

								<c:if test = "${user.is_stopped == 0}" >
									<form action="is_stopped" method="post" onSubmit="return userStop()"><br />
										<input type="hidden" name="user_id" value=${user.id } /> <br />
			 							<input type="hidden" name="is_stopped" value=1 /> <br />
										<input type="submit" value="停止" class="btn" />
									</form>
								</c:if>

								<c:if test = "${user.is_stopped == 1}" >
									<form action="is_stopped" method="post" onSubmit="return revival()"><br />
			 							<input type="hidden" name="user_id" value=${user.id } /> <br />
			 							<input type="hidden" name="is_stopped" value=0 /><br />
										<input type="submit" value="復活" class="btn" /> <br />
									</form>
								</c:if>
							</c:if>
						</div>
						<div class="botton">
							<form action="settings" method="get"><br />
								<input type="hidden" name="user_id" value=${user.id } /> <br />
								<input type="submit" value="編集" class="btn" /> <br />
							</form>
						</div>

						</div>


						</c:forEach>
					</div>
				</div>
			</div>
		</div>
			<a href="./" class="btn-border">ホームに戻る</a>
			<div class="copyright"> Copyright(c)HarukaSugano</div>
        </div>

    </body>
</html>
