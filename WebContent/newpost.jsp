<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>新規投稿</title>

    </head>
    <body>
    	<div class="header">
			<div class="item">
				<div class="item-name">

					<a href="./" class="btn-border"><span>ホーム</span></a>
				</div>
			</div>
		</div>
<br>

        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

<br><br>
<br><br>
<br><br>
			<div class="newpost-items">
	            <form action="newpost" method="post">
	                <label for="title">件名（30文字まで）</label><br>
	                <input name="title" id="title" value="${title}"/> <br /><br>

	                <label for="category">カテゴリ（10文字まで）</label><br>
	                <input name="category" id="category" value="${category}"/> <br /><br>


	                <label for="text">本文（1000文字まで）<br />
	                <textarea name="text" cols="80" rows="8" class="tweet-box"><c:out value="${text}"/></textarea>
	                <br />


	                <input type="submit" value="投稿" class="btn"/> <br />

	            </form>
            <a href="./"  class="btn-border">戻る</a>

            </div>
            <div class="copyright"> Copyright(c)HarukaSugano</div>
        </div>
    </body>
</html>