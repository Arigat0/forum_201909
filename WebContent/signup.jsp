<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新規登録</title>
    <link href="style.css" rel="stylesheet" type="text/css">



    </head>
    <body>
    	<div class="header">
			<div class="item">
				<div class="item-name">
					<a href="usermanagement" class="btn-border"><span>ホーム</span></a>
				</div>
			</div>
		</div>
<br>


        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
<br>
		<div class="um-main">
           	<div class="signupuser">
           		<div class="signups">
		            <form action="signup" method="post">
		                <br /> <label for="name">名前</label><br />
		                <input name="name" id="name" value="${name}" /><br />

		                <label for="account">ログインID</label> <br />
		                <input name="account" id="account" value="${account}" /> <br />

		                <label for="password">パスワード</label> <br />
		                <input name="password" type="password" id="password" /> <br />

		                <label for="passwordConfirm">パスワード確認用</label><br />
		                <input name="passwordConfirm" type="password" id="passwordConfirm" oninput="CheckPassword(this)"/> <br />


						<label for="branch_id">支店</label> <br />
		                <select name="branch_id">

		                	<option value="0">選択してください</option>

		                	<c:forEach items="${branches}" var="branch">
		                		<c:if test="${ branch.id == branch_id }">
		                			<option selected value="${branch.id}">${branch.name}</option>
		                		</c:if>
		                	</c:forEach>

		                	<c:forEach items="${branches}" var="branch">
		                		<c:if test="${ branch.id != branch_id }">
		                			<option value="${branch.id}">${branch.name}</option>
		                		</c:if>
		                	</c:forEach>

		                </select>

						</p>

		                <label for="position_id">部署・役職</label><br />
		                <select name="position_id" value="${position_id}">${position.position}
		                	<option value="0">選択してください</option>


		                	<c:forEach items="${positions}" var="position">
		                		<c:if test="${position.id == position_id }">
		                		<option selected value="${position.id}">${position.position}</option>
		                		</c:if>
		                	</c:forEach>

		                	<c:forEach items="${positions}" var="position">
		                		<c:if test="${position.id != position_id }">
		                		<option value="${position.id}">${position.position}</option>
		                		</c:if>
		                	</c:forEach>

		                </select>
	<br>
		                <br /> <input type="submit" value="登録" class="btn"/> <br />

				  </form>
	                <a href="usermanagement" class="btn-border">戻る</a>
					</div>
	            <div class="copyright">Copyright(c)HarukaSugano</div>
        	</div>
        </div>
    </body>
</html>
