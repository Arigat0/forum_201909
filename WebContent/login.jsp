<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>

        <link href="style.css" rel="stylesheet" type="text/css">

        <style type="text/css">
<!--
		.header {
			height: 25px;
			width: 100%;
			background-color:#666699;
			position: fixed;
			top:0;
			z-index:10;
		}
		.loginmain {
			height:450px;
			width: 200px;
			margin:0 auto;
		}

-->
		</style>


    </head>
    <body>
    	<div class="header">
			<div class="item">
				<div class="item-name">


				</div>
			</div>
		</div>
<br>

        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
<br>
<br>
<br>
			<div class="loginmain">
	            <form action="login" method="post"><br />
	                <label for="account">ログインID</label>
	                <input name="account" id="account" value="${account}" /> <br />
	                <c:remove var="account" scope="session"/>

	                <label for="password">パスワード</label>
	                <input name="password" type="password" value="${password}"/> <br />
<br>
	                <input type="submit" value="ログイン" class="btn" /> <br />


	  <!--               <a href="./">戻る</a> -->
	            </form>
	            <div class="copyright"> Copyright(c)HarukaSugano</div>
	        </div>
        </div>
    </body>
</html>