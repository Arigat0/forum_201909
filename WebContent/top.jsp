<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<% pageContext.setAttribute("newLine", "\n"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="style.css" rel="stylesheet" type="text/css">
        <title>ホーム</title>
        <script type="text/javascript">

		    function deletingPost(){
        		if(window.confirm("削除しますか？")) {
        			return true;
        		}else{
        			return false;
        		}
        	}
		</script>

		<style type="text/css"></style>
    </head>
    <body>
        <div class="main-contents">
        	<c:if test="${ not empty errorMessages }">
        		<div class="errorMessages">
        			<ul>
        				<c:forEach items="${errorMessages}" var="message">
        					<li><c:out value="${message}" />
        				</c:forEach>
        			</ul>
        		</div>
        		<c:remove var="errorMessages" scope="session"/>
        	</c:if>
<br>
<br>
            <div class="header">
				<div class="item">
					<div class="item-name">

						<a href="./" class="btn-border"><span>ホーム</span></a>
					</div>
				</div>
				<div class="item">
					<div class="item-name">
						<a href="newpost" class="btn-border"><span>新規投稿</span></a>
					</div>
				</div>
				<div class="item">
					<div class="item-name">
								<c:if test = "${branch_id == 1 && position_id==1 }">
									<a href="usermanagement" class="btn-border"><span>ユーザ管理</span></a>
								</c:if>
					</div>
				</div>
				<div class="item">
					<div class="item-name">
						<a href="logout" class="btn-border"><span>ログアウト</span></a>
					</div>
				</div>
			</div>
<br>
<br>
<br>
			<div class="search">
				<div class="search-box">
					<form action="./" method="get"><br>
						<div class="search-item">
								<label for="term" style="color:white; font-family='arial'">検索期間</label>
								<input type="date" name="startTime" value="${startTime}"></input>
								<input type="date" name="endTime" value="${endTime}"></input>

								<label for="term" style="color:white; font-family='arial'">検索ワード</label>
								<input name="searchWord" value="${searchWord}" />

								<input type="submit" value="検索"  class="btn" /><br>
						</div>
					</form>
				</div>
			</div>
<br>
<br>
		<div class="form-area">
			<div class="posts">
				<div class="onepost">
					<c:forEach items="${posts}" var="post">
						<div class="post">
							<div class="account-name">
							 	<span class="account">投稿者 <c:out value="${post.account}" /></span>
							 	<span class="name"><c:out value="${post.name}" /></span>
							</div>
<br>
							<label for="title">件名<br />
							<div class="title"><h2><c:out value="${post.title}" /></h2></div><br>

							<label for="category">カテゴリー
							<div class="category"><h3><c:out value="${post.category}" /></h3></div>
<br>
							<div class="text">
							<c:forEach var="pst" items="${fn:split(post.text, newLine)}">
								<c:out value="${pst}"/><br/>
							</c:forEach>
							</div>

							<div class="date"><fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>


							<c:if test = "${loginUser.id == post.userId}">
								<form action="postDelete" method="post" onSubmit="return deletingPost();"><br />
									<input type="hidden" name="post_id" value=${post.id } /> <br />
									<input type="submit" value="削除" class="btn" /> <br />
								</form>
							</c:if>
						</div>
						<div class="commentpost">
							 <form action="comment" method="post"><br />
							 	<label for="comment">コメント<br />
							 	<textarea name="text" cols="80" rows="5" class="tweet-box"></textarea>
							 	<br />（500文字まで）
							 	<input type="hidden" name="post_id" value=${post.id } /> <br />

							 	<input type="submit" value="コメント" class="btn"/> <br />
							 </form>
						</div>
<br>

						<div class="pastcomment">
							 <c:forEach items="${comments}" var ="comment">

							 	<div class="comment">
									<c:if test="${post.id == comment.post_id}" >

										<div class="text">
										<c:forEach var="cmt" items="${fn:split(comment.text, newLine)}">
											<c:out value="${cmt}"/><br/>
										</c:forEach>
										</div>

										<div class="account-name"> 投稿者
										<c:out value="${comment.account}" /> </span>
										<span class="name">
										<c:out value="${comment.name}" /> </span>
										</div>
										<div class="date"> <fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
<br>
<br>


								<!-- 	${loginUser.id}　topServlet内に既に取得しているデータを使う
										${comment.userId }
										${comment.id } -->
										<div class="delete">
											<c:if test = "${loginUser.id == comment.userId}">
												<form action="commentDelete" method="post" onSubmit="return deletingPost()"><br />
													<input type="hidden" name="comment_id" value=${comment.id } /> <br />
													<input type="submit" value="削除" class="btn"/> </span><br>
												</form>
											</c:if>
										</div>
									</c:if>
								</div>
							</c:forEach>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>

            <div class="copyright"> Copyright(c)HarukaSugano</div>
        </div>
    </body>
</html>