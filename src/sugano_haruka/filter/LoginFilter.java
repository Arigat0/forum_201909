package sugano_haruka.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sugano_haruka.beans.User;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter("/*")
public class LoginFilter implements Filter {

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		if (((HttpServletRequest)request).getServletPath().equals("/login") || ((HttpServletRequest)request).getServletPath().contains("/css")) {
			chain.doFilter(request, response);

		}else {
			HttpSession session = ((HttpServletRequest)request).getSession();
			User loginCheck = (User)session.getAttribute("loginUser");

			if (loginCheck != null) {
				chain.doFilter(request, response);

			}else {
				System.out.println("ログイン"+((HttpServletRequest)request).getServletPath());
				List<String> errorMessages = new ArrayList<String>();
				errorMessages.add("ログインしてください");
				session.setAttribute("errorMessages", errorMessages);

				((HttpServletResponse)response).sendRedirect("login");
			}
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

	}
	public void destroy() {

	}
}
