package sugano_haruka.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sugano_haruka.beans.User;


/**
 * Servlet Filter implementation class AuthorityFilter
 */
@WebFilter(urlPatterns = { "/usermanagement", "/settings", "/signup"})

public class AuthorityFilter implements Filter {


	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		User user= (User)session.getAttribute("loginUser");
//get method doesn't need argument


		if (user != null) {
			int branchCheck = user.getBranch_id();
			int positionCheck = user.getPosition_id();

			if(branchCheck == 1 && positionCheck ==1) {
				chain.doFilter(request, response);

			}else if(branchCheck == 1 && positionCheck ==2){

				List<String> messages = new ArrayList<String>();
				messages.add("アクセス権限がありません");
				session.setAttribute("errorMessages", messages);

				((HttpServletResponse)response).sendRedirect("./");

			}else {
				List<String> messages = new ArrayList<String>();
				messages.add("アクセス権限がありません");
				session.setAttribute("errorMessages", messages);

				((HttpServletResponse)response).sendRedirect("./");
			}
		}else {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse)response).sendRedirect("login");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	public void destroy() {
		// TODO Auto-generated method stub
	}
}
