package sugano_haruka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sugano_haruka.beans.Branch;
import sugano_haruka.beans.Position;
import sugano_haruka.beans.User;
import sugano_haruka.service.BranchService;
import sugano_haruka.service.PositionService;
import sugano_haruka.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	List<Branch> allBranches = new BranchService().getBranches();
    	List<Position> allPositions = new PositionService().getPositions();

    	request.setAttribute("branches", allBranches);
    	request.setAttribute("positions", allPositions);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        User user = new User();

        user.setName(request.getParameter("name"));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setBranch_id(Integer.valueOf(request.getParameter("branch_id")));
        user.setPosition_id(Integer.valueOf(request.getParameter("position_id")));



        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            new UserService().register(user);

            response.sendRedirect("usermanagement");
        } else {

        	String branch_name = request.getParameter("selected_br_name");


        	request.setAttribute("name", user.getName());
        	request.setAttribute("account", user.getAccount());
        	request.setAttribute("branch_name", branch_name);
        	request.setAttribute("branch_id", user.getBranch_id());
        	request.setAttribute("position_id", user.getPosition_id());

        	List<Branch> allBranches = new BranchService().getBranches();
        	List<Position> allPositions = new PositionService().getPositions();

        	request.setAttribute("branches", allBranches);
        	request.setAttribute("positions", allPositions);



            session.setAttribute("errorMessages", messages);

            request.getRequestDispatcher("/signup.jsp").forward(request, response);
 //           response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String name = request.getParameter("name");
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("passwordConfirm");
        int branch_id = Integer.valueOf(request.getParameter("branch_id"));
        int position_id = Integer.valueOf(request.getParameter("position_id"));
        UserService userService = new UserService();
        User user = userService.getAccount(account);

        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(name) == false && StringUtils.length(name) > 10) {
        	messages.add("名前は10文字以下で入力してください");
        }

        if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (account.matches("^[!-~]{1,5}$")) {
        	messages.add("ログインIDは6文字以上・半角英数字で入力してください");
        }
        if (account.matches("^[!-~]{21,}$")) {
        	messages.add("ログインIDは20文字以下・半角英数字で入力してください");
        }
        if (user != null) {
        	messages.add("ログインIDが重複しています");
        }


        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (password.matches("^[!-~¥]{1,5}$")) {
        	messages.add("パスワードは6文字以上で入力してください");
        }
        if (password.matches("^[!-~¥]{21,}$")) {
        	messages.add("パスワードは20文字以下で入力してください");
        }
        if (password.matches("^\\s+")) {
        	messages.add("パスワードは記号を含む全ての半角文字で入力してください");
        }
        if(!password.equals(passwordConfirm) ) {
        	messages.add("パスワードと確認用パスワードが一致しません");
        }

        if (branch_id== 0) {
            messages.add("支店を選択してください");
        }
        if (position_id == 0) {
            messages.add("部署・役職を選択してください");
        }


        if (branch_id == 1 && position_id == 3) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 1 && position_id == 4) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 3 && position_id == 1) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 3 && position_id == 2) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 4 && position_id == 1) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 4 && position_id == 2) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 5 && position_id == 1) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 5 && position_id == 2) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}