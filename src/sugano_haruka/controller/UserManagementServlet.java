package sugano_haruka.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sugano_haruka.beans.Branch;
import sugano_haruka.beans.Position;
import sugano_haruka.beans.User;
import sugano_haruka.service.BranchService;
import sugano_haruka.service.PositionService;
import sugano_haruka.service.UserService;

/**
 * Servlet implementation class UserManagementServlet
 */
@WebServlet("/usermanagement")
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

//Filterで総務だけが見られるようにしている
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		User user = (User)request.getSession().getAttribute("loginUser");
		boolean isShowUserManagement;
		if (user != null) {
			isShowUserManagement = true;
		}else {
			isShowUserManagement = false;
		}

    	List<User> allUsers = new UserService().getAllUsers();
    	List<Branch> allBranches = new BranchService().getBranches();
    	List<Position> allPositions = new PositionService().getPositions();


    	request.setAttribute("user_id", user.getId());
    	request.setAttribute("users", allUsers);
    	request.setAttribute("branches", allBranches);
    	request.setAttribute("positions", allPositions);
    	request.setAttribute("isShowUserManagement", isShowUserManagement);

    	request.getRequestDispatcher("/usermanagement.jsp").forward(request, response);
	}
}
