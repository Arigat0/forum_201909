package sugano_haruka.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sugano_haruka.beans.Comment;
import sugano_haruka.beans.User;
import sugano_haruka.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

// doGet for showing the posts
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	request.getRequestDispatcher("/comment.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> comments = new ArrayList<String>();
        HttpSession session = request.getSession();

        if (isValid(request, comments) == true) {

            User user = (User) session.getAttribute("loginUser");


            Comment comment = new Comment();
            comment.setUserId(user.getId());
            comment.setText(request.getParameter("text"));
            comment.setPost_Id(Integer.valueOf(request.getParameter("post_id")));

            new CommentService().register(comment);


//response.sendRedirect("./") means it calls back to top.jsp
            response.sendRedirect("./");
        } else {

            session.setAttribute("errorMessages", comments);
            response.sendRedirect("./");
        }
    }
    private boolean isValid(HttpServletRequest request, List<String> comments) {

        String comment = request.getParameter("text");

        if (StringUtils.isBlank(comment) == true) {
            comments.add("コメントを入力してください");
        }
        if (500 < comment.length()) {
            comments.add("500文字以下で入力してください");
        }
        if (comments.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}