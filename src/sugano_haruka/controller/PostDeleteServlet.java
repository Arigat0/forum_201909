package sugano_haruka.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sugano_haruka.beans.Post;
import sugano_haruka.service.PostService;


/**
 * Servlet implementation class PostDeleteServlet
 */
@WebServlet("/postDelete")
public class PostDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        Post postDelete = new Post();
        postDelete.setId(Integer.valueOf(request.getParameter("post_id")));

        new PostService().postDelete(postDelete);


        response.sendRedirect("./");

    }
}
