package sugano_haruka.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sugano_haruka.beans.Comment;
import sugano_haruka.service.CommentService;


/**
 * Servlet implementation class PostDeleteServlet
 */
@WebServlet("/commentDelete")
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        Comment commentDelete = new Comment();
        commentDelete.setId(Integer.valueOf(request.getParameter("comment_id")));
        new CommentService().commentDelete(commentDelete);


        response.sendRedirect("./");

    }
}
