package sugano_haruka.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sugano_haruka.beans.User;
import sugano_haruka.beans.UserComment;
import sugano_haruka.beans.UserPost;
import sugano_haruka.service.CommentService;
import sugano_haruka.service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	User user = (User)request.getSession().getAttribute("loginUser");


    	boolean isShowPostForm;
    	if (user != null) {
    		isShowPostForm = true;
    	}else {
    		isShowPostForm = false;
    	}


    	String startTime = request.getParameter("startTime");
    	String endTime = request.getParameter("endTime");
    	String searchWord = request.getParameter("searchWord");
    	int branch_id = user.getBranch_id();
    	int position_id = user.getPosition_id();

    	List<UserPost> posts = new PostService().getPost(startTime, endTime, searchWord);
    	List<UserComment> comments = new CommentService().getComment();


    	request.setAttribute("posts", posts);
    	request.setAttribute("comments", comments);
    	request.setAttribute("isShowPostForm", isShowPostForm);
    	request.setAttribute("startTime", startTime);
    	request.setAttribute("endTime", endTime);
    	request.setAttribute("searchWord", searchWord);
    	request.setAttribute("branch_id", branch_id);
    	request.setAttribute("position_id", position_id);

    	request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}