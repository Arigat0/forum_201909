package sugano_haruka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sugano_haruka.beans.Post;
import sugano_haruka.beans.User;
import sugano_haruka.service.PostService;

@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

// doGet for showing the posts
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	request.getRequestDispatcher("newpost.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        User user = (User) session.getAttribute("loginUser");
        List<String> posts = new ArrayList<String>();
        Post post = new Post();
        post.setUserId(user.getId());
        post.setTitle(request.getParameter("title"));
        post.setCategory(request.getParameter("category"));
        post.setText(request.getParameter("text"));



        if (isValid(request, posts) == true) {

            new PostService().register(post);

            response.sendRedirect("./");
        }else {
        	request.setAttribute("title", post.getTitle());
        	request.setAttribute("category", post.getCategory());
        	request.setAttribute("text", post.getText());
        	session.setAttribute("errorMessages", posts);

   //         response.sendRedirect("newpost");
        	request.getRequestDispatcher("/newpost.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> posts) {

        String title = request.getParameter("title");
        String category = request.getParameter("category");
        String text = request.getParameter("text");

        if (StringUtils.isBlank(title) == true) {
            posts.add("件名を入力してください");
        }
        if (StringUtils.isBlank(category) == true) {
            posts.add("カテゴリーを入力してください");
        }
        if (StringUtils.isBlank(text) == true) {
            posts.add("本文を入力してください");
        }
        if (30 < title.length()) {
            posts.add("件名は30文字以下で入力してください");
        }
        if (10 < category.length()) {
            posts.add("カテゴリーは10文字以下で入力してください");
        }
        if (1000 < text.length()) {
            posts.add("本文は1000文字以下で入力してください");
        }
        if (posts.size() == 0) {
            return true;
        }else {
            return false;
        }
    }

}