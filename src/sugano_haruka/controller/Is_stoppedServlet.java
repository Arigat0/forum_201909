package sugano_haruka.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sugano_haruka.beans.User;
import sugano_haruka.service.UserService;

@WebServlet(urlPatterns = { "/is_stopped" })
public class Is_stoppedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		User userControl = new User();
		userControl.setId(Integer.valueOf(request.getParameter("user_id")));
		userControl.setIs_stopped(Integer.valueOf(request.getParameter("is_stopped")));

		new UserService().userControl(userControl);

		response.sendRedirect("/sugano_haruka/usermanagement");
	}

}
