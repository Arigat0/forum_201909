package sugano_haruka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sugano_haruka.beans.Branch;
import sugano_haruka.beans.Position;
import sugano_haruka.beans.User;
import sugano_haruka.service.BranchService;
import sugano_haruka.service.PositionService;
import sugano_haruka.service.UserService;


/**
 * Servlet implementation class SettingsServlet
 */
@WebServlet("/settings")
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		User loginUser = (User)request.getSession().getAttribute("loginUser");
		int loginUser_id = loginUser.getId();
		HttpSession session = request.getSession();

		String userId = request.getParameter("user_id");


		try {
			if( StringUtils.isBlank(userId) == true) {
				List<String> blockMessages = new ArrayList<String>();
	            blockMessages.add("不正なパラメータが入力されました");
	            session.setAttribute("errorMessages", blockMessages);
	            response.sendRedirect("usermanagement");

			}else {

				if( !userId.matches("^[0-9]*$")) {

					List<String> blockMessages = new ArrayList<String>();
		            blockMessages.add("不正なパラメータが入力されました");
		            session.setAttribute("errorMessages", blockMessages);
		            response.sendRedirect("usermanagement");
				}else {


					UserService userService = new UserService();
			        User user = userService.getUser(Integer.valueOf(userId));

					if(user == null) {
						List<String> blockMessages = new ArrayList<String>();
			            blockMessages.add("不正なパラメータが入力されました");
			            session.setAttribute("errorMessages", blockMessages);
			            response.sendRedirect("usermanagement");
					}else {

				        String account = user.getAccount();
						String name = user.getName();
						int branch_id = user.getBranch_id();
						int position_id = user.getPosition_id();

						BranchService branchService = new BranchService();
						Branch branch = branchService.branchName(branch_id);
						String branchName = branch.getName();

						PositionService positionSevice = new PositionService();
						Position position = positionSevice.positionName(position_id);
						String positionName = position.getPosition();

						List<Branch> allBranches = new BranchService().getBranches();
						List<Position> allPositions = new PositionService().getPositions();

						//User_id from DB

						request.setAttribute("userId", userId);
						//logged-in User's Id
						request.setAttribute("userid", loginUser_id);
						request.setAttribute("account", account);
						request.setAttribute("name", name);
						request.setAttribute("branch_id", branch_id);
						request.setAttribute("position_id", position_id);
						request.setAttribute("branches", allBranches);
						request.setAttribute("positions", allPositions);
						request.setAttribute("branchName", branchName);
						request.setAttribute("positionName", positionName);

						request.getRequestDispatcher("settings.jsp").forward(request,response);
					}
				}
			}
		}catch (NumberFormatException e){
			List<String> blockMessages = new ArrayList<String>();
            blockMessages.add("不正なパラメータが入力されました");
            session.setAttribute("errorMessages", blockMessages);
            response.sendRedirect("usermanagement");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String passwordConfirm = request.getParameter("passwordConfirm");
		User user_info = getEditUser(request);
		String account = user_info.getAccount();
		String name = user_info.getName();
		int branch_id = user_info.getBranch_id();
		int position_id = user_info.getPosition_id();

		List<Branch> allBranches = new BranchService().getBranches();
		List<Position> allPositions = new PositionService().getPositions();
		List<String> errorMessage = new ArrayList<String>();


		if (isValid(user_info, passwordConfirm, errorMessage) == true) {

			new UserService().update(user_info);

			response.sendRedirect("/sugano_haruka/usermanagement");

		}else {
			request.setAttribute("branches", allBranches);
			request.setAttribute("positions", allPositions);
			request.setAttribute("userId", user_info.getId());
			request.setAttribute("account", account);
			request.setAttribute("name", name);
			request.setAttribute("branch_id", branch_id);
			request.setAttribute("position_id", position_id);

			request.setAttribute("errorMessages", errorMessage);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
			response.sendRedirect("settings");

		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.valueOf(request.getParameter("userId")));
		editUser.setName(request.getParameter("name"));
		editUser.setAccount(request.getParameter("account"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch_id(Integer.valueOf(request.getParameter("branch_id")));
		editUser.setPosition_id(Integer.valueOf(request.getParameter("position_id")));
		return editUser;
	}

    private boolean isValid(User editUser, String passwordConfirm,  List<String> messages) {

        int id = editUser.getId();
    	String account = editUser.getAccount();
        String name = editUser.getName();
        String password = editUser.getPassword();
        int branch_id = editUser.getBranch_id();
        int position_id = editUser.getPosition_id();
        UserService userService = new UserService();
        User user = userService.getAccount(account);




        if (StringUtils.isEmpty(name) == false && StringUtils.length(name) > 10) {
        	messages.add("名前は10文字以下で入力してください");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
//        if (StringUtils.isEmpty(passwordConfirm) == true) {
//            messages.add("確認用パスワードを入力してください");
//        }

        if (StringUtils.isEmpty(account) == true) {
            messages.add("ログインIDを入力してください");
        }

        if (user != null && user.getId() != id) {
        	messages.add("ログインIDが重複しています");
        }
        if (account.matches("^[!-~]{1,5}$")) {
        	messages.add("ログインIDは6文字以上で入力してください");
        }
        if (account.matches("^[!-~]{21,}$")) {
        	messages.add("ログインIDは20文字以下で入力してください");
        }


        if(!password.equals(passwordConfirm) ) {
        	messages.add("パスワードと確認用パスワードが一致しません");
        }




        if (StringUtils.isEmpty(password) == false && password.matches("^[!-~]{1,5}$")) {
        	messages.add("パスワードは6文字以上で入力してください");
        }

        if (StringUtils.isEmpty(password) == false && password.matches("^[!-~]{21,}$")) {
        	messages.add("パスワードは20文字以下で入力してください");
        }
        if (password.matches("^\\s+")) {
        	messages.add("パスワードは記号を含む全ての半角文字で入力してください");
        }


        if (branch_id== 0) {
            messages.add("支店を選択してください");
        }
        if (position_id == 0) {
            messages.add("部署・役職を選択してください");
        }
        if (branch_id == 1 && position_id == 3) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 1 && position_id == 4) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 3 && position_id == 1) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 3 && position_id == 2) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 4 && position_id == 1) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 4 && position_id == 2) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 5 && position_id == 1) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if (branch_id == 5 && position_id == 2) {
            messages.add("支店と部署・役職の組み合わせが不正です");
        }





        if (messages.size() == 0) {
            return true;
        }else {
            return false;
        }
    }

}