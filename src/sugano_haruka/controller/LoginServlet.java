package sugano_haruka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sugano_haruka.beans.User;
import sugano_haruka.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<String> loginError = new ArrayList<String>();
    	HttpSession session = request.getSession();

    	String account = request.getParameter("account");
	    String password = request.getParameter("password");

	   	if (isValid(request,loginError) == true) {


//Userの中のis_stoppedのデータを呼んでくる

	        LoginService loginService = new LoginService();
	        User user = loginService.login(account, password);
	        int is_stopped = user.getIs_stopped();


	        if (user != null && is_stopped == 0) {

	            session.setAttribute("loginUser", user);
	            response.sendRedirect("./");

	        }else {
	            List<String> blockMessages = new ArrayList<String>();
	            blockMessages.add("ログインIDまたはパスワードが間違っています");
	            session.setAttribute("errorMessages", blockMessages);
	            response.sendRedirect("login");
	        }

       }else {
    	   session.setAttribute("account", account);
    	   session.setAttribute("errorMessages", loginError);
    	   response.sendRedirect("login");
       }
    }

    private boolean isValid(HttpServletRequest request, List<String> message) {

        String account = request.getParameter("account");
        String password = request.getParameter("password");
        LoginService loginService = new LoginService();
        User user = loginService.login(account, password);



        if (StringUtils.isEmpty(account) == true && StringUtils.isEmpty(password) == false) {
            message.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(account) == false && StringUtils.isEmpty(password) == true) {
            message.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(account) == true && StringUtils.isEmpty(password) == true) {
        	message.add("ログインIDまたはパスワードが入力されていません");
        }
        if (user == null && StringUtils.isEmpty(account) == false && StringUtils.isEmpty(password) == false ) {
      	message.add("ログインIDまたはパスワードが間違っています");
        }





        if (message.size() == 0) {
            return true;
        }else {
            return false;
        }
    }
}