package sugano_haruka.dao;

import static sugano_haruka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import sugano_haruka.beans.Comment;
import sugano_haruka.exception.SQLRuntimeException;




public class CommentDao {
	public void insert(Connection connection, Comment comment) {

	    PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("INSERT INTO comments ( ");
	        sql.append("user_id");
	        sql.append(", post_id");
	        sql.append(", text");
	        sql.append(", created_date");
	        sql.append(", updated_date");
	        sql.append(") VALUES (");
	        sql.append(" ?"); // user_id
	        sql.append(", ?"); // post_Id
	        sql.append(", ?"); // text
	        sql.append(", CURRENT_TIMESTAMP"); // created_date
	        sql.append(", CURRENT_TIMESTAMP"); // updated_date
	        sql.append(")");

	        ps = connection.prepareStatement(sql.toString());

	        ps.setInt(1, comment.getUserId());
	        ps.setInt(2, comment.getPost_Id());
	        ps.setString(3, comment.getText());

	        ps.executeUpdate();
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
	public void commentDelete(Connection connection, Comment commentDelete) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE id=?");
			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentDelete.getId());

			ps.executeUpdate();

		}catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}
}
