package sugano_haruka.dao;
import static sugano_haruka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import sugano_haruka.beans.Post;
import sugano_haruka.exception.SQLRuntimeException;


public class PostDao {

	public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("user_id");
            sql.append(", title");
            sql.append(", category");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // title
            sql.append(", ?"); // category
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getUserId());
            ps.setString(2, post.getTitle());
            ps.setString(3, post.getCategory());
            ps.setString(4, post.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

//削除指示

	public void postDelete(Connection connection, Post postDelete) {
		 PreparedStatement ps = null;
		 try {
            StringBuilder sql = new StringBuilder();

            sql.append("DELETE FROM posts WHERE id=?");
            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, postDelete.getId());

            ps.executeUpdate();

		 }catch(SQLException e) {
			 throw new SQLRuntimeException(e);
		 }finally {
            close(ps);
        }
	}

}
