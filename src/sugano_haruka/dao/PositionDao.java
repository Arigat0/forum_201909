package sugano_haruka.dao;

import static sugano_haruka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sugano_haruka.beans.Position;
import sugano_haruka.exception.SQLRuntimeException;


public class PositionDao {
	public List<Position> getPositions(Connection connection){

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM positions";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Position> positionList = toPositionList(rs);
			if(positionList.isEmpty() == true) {
				return null;
			}else {
				return positionList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
	private List<Position>toPositionList(ResultSet rs) throws SQLException {
		List<Position> ret = new ArrayList<Position>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String positionName = rs.getString("position");

				Position position = new Position();
				position.setId(id);
				position.setPosition(positionName);

				ret.add(position);
			}
			return ret;
		}finally {
			close(rs);
		}
	}

	public Position positionName(Connection connection, int position_id) {


		PreparedStatement ps = null;
    	try {

    		String sql = "SELECT * FROM positions WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, position_id);

    		ResultSet rs = ps.executeQuery();
    		List<Position> positionList = toPositionList(rs);
    		if(positionList.isEmpty() == true) {
				return null;
			}else {
				return positionList.get(0);
			}
    	}catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	}finally {
    		close(ps);
    	}



	}
}
