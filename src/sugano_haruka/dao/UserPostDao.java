package sugano_haruka.dao;

import static sugano_haruka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import sugano_haruka.beans.UserPost;
import sugano_haruka.exception.SQLRuntimeException;


public class UserPostDao {
	public List<UserPost> getUserPosts(Connection connection ,String startTime, String endTime, String searchWord) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.title as title, ");
            sql.append("posts.category as category, ");
            sql.append("posts.text as text, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");

            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");

            sql.append("WHERE posts.created_date BETWEEN ? AND ? ");
            if( searchWord != null && searchWord.isEmpty() == false) {
            	 sql.append("AND category LIKE ?");
            }

            sql.append("ORDER BY posts.created_date DESC ");


            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, startTime);
            ps.setString(2, endTime);
            if(searchWord != null && searchWord.isEmpty() == false) {
            	ps.setString(3, "%" + searchWord+ "%");
            }

            System.out.println(ps);

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String title = rs.getString("title");
                String category = rs.getString("category");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserPost post = new UserPost();
                post.setAccount(account);
                post.setName(name);
                post.setId(id);
                post.setUserId(userId);
                post.setTitle(title);
                post.setCategory(category);
                post.setText(text);
                post.setCreated_date(createdDate);
                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
