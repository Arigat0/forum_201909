package sugano_haruka.dao;

import static sugano_haruka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import sugano_haruka.beans.User;
import sugano_haruka.exception.NoRowsUpdatedRuntimeException;
import sugano_haruka.exception.SQLRuntimeException;


public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String account,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ?  AND password = ? ";

            ps = connection.prepareStatement(sql);
            ps.setString(1, account);
            ps.setString(2, password);

//toUserListへ
            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            }else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            }else {
                return userList.get(0);
            }
        }catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }finally {
            close(ps);
        }
    }


//Beansの中へ
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int is_stopped = rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setIs_stopped(is_stopped);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        }finally {
            close(rs);
        }
    }

    public List<User> getAllUsers(Connection connection) {
    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users ";

    		ps = connection.prepareStatement(sql);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toAllUsersList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		}else {
    			return userList;
    		}
    	}catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	}finally {
    		close(ps);
    	}
    }
    private List<User> toAllUsersList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int is_stopped = rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setAccount(account);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setIs_stopped(is_stopped);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        }finally {
            close(rs);
        }
    }


    public void userControl(Connection connection,User userControl) {
    	PreparedStatement ps = null;

    	try {
    		StringBuilder sql = new StringBuilder();

    			sql.append("UPDATE users SET is_stopped = ? WHERE id=?");
    			ps = connection.prepareStatement(sql.toString());


    			ps.setInt(1, userControl.getIs_stopped());
    			ps.setInt(2, userControl.getId());

    			ps.executeUpdate();

    	}catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
    }

    public User getUserId(Connection connection, int editUser_id) {

    	PreparedStatement ps = null;
    	try {

    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, editUser_id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		}else {
    			return userList.get(0);
    		}
    	}catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	}finally {
    		close(ps);
    	}
    }


    public User getEditUser(Connection connection, User getEditUser) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, getEditUser.getId());

            ResultSet rs = ps.executeQuery();
            List<User> editUserList = toUserList(rs);
            if(editUserList.isEmpty() == true) {
				return null;
			}else {
				return editUserList.get(0);
			}

        }catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }finally {
            close(ps);
        }
    }
    public void update(Connection connection, User getEditUser) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  account = ?");
            sql.append(", name = ?");
            //if password isn't empty, it will be updated.
            if (StringUtils.isEmpty(getEditUser.getPassword()) == false) {
            	sql.append(", password = ?");
            }

            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, getEditUser.getAccount());
            ps.setString(2, getEditUser.getName());
            //if password isn't empty, password is also updated.
            if (StringUtils.isEmpty(getEditUser.getPassword()) == false) {
	            ps.setString(3, getEditUser.getPassword());
	            ps.setInt(4, getEditUser.getBranch_id());
	            ps.setInt(5, getEditUser.getPosition_id());
	            ps.setInt(6, getEditUser.getId());
            }
            //if password is empty,it won't be updated, so we can skip number and insert in DB.
            else {
            	ps.setInt(3, getEditUser.getBranch_id());
            	ps.setInt(4, getEditUser.getPosition_id());
	            ps.setInt(5, getEditUser.getId());

            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        }catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }finally {
            close(ps);
        }
    }


    public User getUser(Connection connection, int user_id) {

    	PreparedStatement ps = null;
    	try {

    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, user_id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		}else {
    			return userList.get(0);
    		}
    	}catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	}finally {
    		close(ps);
    	}
    }




    public User getAccount(Connection connection, String account) {
    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE account = ? ";
    		ps = connection.prepareStatement(sql);
            ps.setString(1, account);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            }else {
                return userList.get(0);
            }

    	}catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }finally {
            close(ps);
        }
    }

}