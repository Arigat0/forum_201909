package sugano_haruka.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String account;
    private String password;
    private String name;
    private int branch_id;
    private int position_id;
    private int is_stopped;
    private Date createdDate;
    private Date updatedDate;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getBranch_id() {
        return branch_id;
    }
    public void setBranch_id(int branch_id) {
        this.branch_id = branch_id;
    }
    public int getPosition_id() {
        return position_id;
    }
    public void setPosition_id(int position_id) {
        this.position_id = position_id;
    }
    public int getIs_stopped() {
        return is_stopped;
    }
    public void setIs_stopped(int is_stopped) {
        this.is_stopped = is_stopped;
    }
    public Date getCreatedDate() {
        return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
    public Date getUpdatedDate() {
        return updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

}
