package sugano_haruka.service;

import static sugano_haruka.utils.CloseableUtil.*;
import static sugano_haruka.utils.DBUtil.*;

import java.sql.Connection;

import sugano_haruka.beans.User;
import sugano_haruka.dao.UserDao;
import sugano_haruka.utils.CipherUtil;



public class LoginService {

	 public User login(String account, String password) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            UserDao userDao = new UserDao();
	            String encPassword = CipherUtil.encrypt(password);
	            User user = userDao.getUser(connection, account, encPassword);

	            commit(connection);

	            return user;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }



}
