package sugano_haruka.service;

import static sugano_haruka.utils.CloseableUtil.*;
import static sugano_haruka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sugano_haruka.beans.Comment;
import sugano_haruka.beans.UserComment;
import sugano_haruka.dao.CommentDao;
import sugano_haruka.dao.UserCommentDao;

public class CommentService {


	public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

//	private static final int LIMIT_NUM = 1000;
//TODO DBから持ってくるデータ数の指定なので今回は使わないのであとでなくす

	public List<UserComment> getComment(){
		Connection connection = null;
	    try {
	        connection = getConnection();

	        UserCommentDao commentDao = new UserCommentDao();
	        List<UserComment> ret = commentDao.getUserComments(connection);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public void commentDelete(Comment commentDelete) {
		Connection connection = null;
		try {
			connection = getConnection();
			CommentDao commentDao = new CommentDao();
			commentDao.commentDelete(connection, commentDelete);

			commit(connection);
		}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
