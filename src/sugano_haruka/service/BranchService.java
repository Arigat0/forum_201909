package sugano_haruka.service;

import static sugano_haruka.utils.CloseableUtil.*;
import static sugano_haruka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sugano_haruka.beans.Branch;
import sugano_haruka.dao.BranchDao;



public class BranchService {
	public List<Branch> getBranches(){
		Connection connection = null;

		try {
			connection = getConnection();

			BranchDao branchesDao = new BranchDao();
			List<Branch> ret = branchesDao.getBranches(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	 public Branch branchName(int branch_id) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		BranchDao branchDao = new BranchDao();
    		Branch branch = branchDao.branchName(connection, branch_id);

    		commit(connection);

    		return branch;
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
