package sugano_haruka.service;

import static sugano_haruka.utils.CloseableUtil.*;
import static sugano_haruka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import sugano_haruka.beans.User;
import sugano_haruka.dao.UserDao;
import sugano_haruka.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
//UserDaoのAllUserの呼び出しメソッド


    public List<User> getAllUsers(){
		Connection connection = null;

	    try {
	        connection = getConnection();

	        UserDao allUsersDao = new UserDao();
	        List<User> ret = allUsersDao.getAllUsers(connection);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
    public void userControl(User userControl) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		UserDao userControlDao = new UserDao();
    		userControlDao.userControl(connection, userControl);

    		commit(connection);
    	}catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
    }




    public void update(User getEditUser) {
    	Connection connection = null;
    	try {
    		connection = getConnection();

    		String encPassword = CipherUtil.encrypt(getEditUser.getPassword());
//When you input new password, it will be updated.and if not,it keeps last password
    		if (StringUtils.isEmpty(getEditUser.getPassword()) == false) {
    			getEditUser.setPassword(encPassword);
    		}

    		UserDao getEditUserDao = new UserDao();
    		getEditUserDao.update(connection, getEditUser);

    		commit(connection);
    	}catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	}catch (Error e) {
    		rollback(connection);
    		throw e;
    	}finally {
    		close(connection);
    	}
    }
    public User getAccount(String account) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		User user = userDao.getAccount(connection, account);

    		commit(connection);

    		return user;
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User getUser(int user_id) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		User user = userDao.getUser(connection, user_id);

    		commit(connection);

    		return user;
    	} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}

