package sugano_haruka.service;

import static sugano_haruka.utils.CloseableUtil.*;
import static sugano_haruka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sugano_haruka.beans.Position;
import sugano_haruka.dao.PositionDao;

public class PositionService {
	public List<Position> getPositions(){
		Connection connection = null;

		try {
			connection = getConnection();

			PositionDao positionsDao = new PositionDao();
			List<Position> ret = positionsDao.getPositions(connection);

			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	 public Position positionName(int position_id) {

	    	Connection connection = null;
	    	try {
	    		connection = getConnection();

	    		PositionDao positionDao = new PositionDao();
	    		Position position = positionDao.positionName(connection, position_id);

	    		commit(connection);

	    		return position;
	    	} catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }
}
