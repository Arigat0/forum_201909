package sugano_haruka.service;

import static sugano_haruka.utils.CloseableUtil.*;
import static sugano_haruka.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import sugano_haruka.beans.Post;
import sugano_haruka.beans.UserPost;
import sugano_haruka.dao.PostDao;
import sugano_haruka.dao.UserPostDao;


public class PostService {

	public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


	public List<UserPost> getPost(String startTime, String endTime, String searchWord){
		Connection connection = null;
	    try {
	        connection = getConnection();

	        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	        if(startTime == null || startTime.isEmpty()) {
	        	startTime = "2019-01-01 00:00:00";
	        }else {
	        	startTime = startTime + " 00:00:00";
	        }

	        if(endTime == null || endTime.isEmpty()) {
	        	endTime = sdf.format(timestamp);
	        }else {
	        	endTime = endTime + " 23:59:59";
	        }

	        UserPostDao postDao = new UserPostDao();
	        List<UserPost> ret = postDao.getUserPosts(connection, startTime, endTime, searchWord);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
//DELETEのためのメソッドを作る
	public void postDelete(Post postDelete) {
		Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.postDelete(connection, postDelete);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

	}
}
