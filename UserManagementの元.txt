protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User)request.getSession().getAttribute("loginUser");
		int user_Br = user.getBranch_id();
		int user_Po = user.getPosition_id();

		boolean isShowUserManagement;
		if(user != null && user_Br == 1 && user_Po == 1 ) {
			isShowUserManagement = true;
		}else if(user != null && user_Br == 1 && user_Po == 2) {
			isShowUserManagement = true;
		}
		else {
			isShowUserManagement = false;
		}
